const exec = require('cordova/exec');

const PLUGIN_NAME = 'BioCatch';

module.exports = {
  // Method to start() BioCatch SDK.
  start: (customerSessionID, wupUrl, publicKey, onSuccess, onError) => {
    exec(onSuccess, onError, PLUGIN_NAME, 'start', [customerSessionID, wupUrl, publicKey]);
  },

  // Method to pause() BioCatch SDK.
  pause: (success, error) => {
    exec(success, error, PLUGIN_NAME, "pause", [])
  },

  // Method to resume() BioCatch SDK.
  resume: (success, error) => {
    exec(success, error, PLUGIN_NAME, "resume", [])
  },

  // Method to stop() BioCatch SDK.
  stop: (success, error) => {
    exec(success, error, PLUGIN_NAME, "stop", [])
  },

  // Method to resetSession() BioCatch SDK.
  resetSession: (success, error) => {
    exec(success, error, PLUGIN_NAME, "resetSession", [])
  },

  // Method to changeContext(newContexttName) BioCatch SDK.
  changeContext: (contextName, success, error) => {
    exec(success, error, PLUGIN_NAME, "changeContext", [contextName])
  },

  // Method to updateCustomerSessionID(newCustomerSessionID) BioCatch SDK.
  updateCustomerSessionID: (customerSessionID, success, error) => {
    exec(success, error, PLUGIN_NAME, "updateCustomerSessionID", [customerSessionID])
  },

  // Method to resetSession() BioCatch SDK.
  generateCustomerSessionNumberID: function (success, error) {
    exec(success, error, PLUGIN_NAME, "generateCustomerSessionNumberID", [])
  },
};
