/**
 */
package com.biocatch;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;

import android.app.Application;
import android.app.Activity;
import android.util.Log;

import java.util.UUID;

import com.biocatch.client.android.sdk.API;

public class BioCatch extends CordovaPlugin {
    private static final String TAG = "BioCatchPlugin";
    private String wuptUrl = null;
    private String customerSessionId = null;
    private String environment = null;
    private Activity currActivity = null;
    private Application currApp = null;
    private String prefix = null;

    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);

        Log.d(TAG, "Inicializando BioCatch Plugin en ambiente");
    }

    public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) throws JSONException {
        final PluginResult result;
        try {
            //  Calling test method
            if (action.equals("test")) {
                Log.d(TAG, "BioCatch test()");
                // An example of returning data back to the web layer
                String phrase = args.getString(0);
                // Echo back the first argument
                result = new PluginResult(PluginResult.Status.OK, "Testing BioCAtch Plugin... " + phrase);
                callbackContext.sendPluginResult(result);
                return true;
            }

            //  Calling start method
            if (action.equals("start") || action.equals("pause") || action.equals("resume") || 
                action.equals("stop") || action.equals("flush") || action.equals("changeContext") ||
                action.equals("updateCustomerSessionID") || action.equals("resetSession") ) {

                this.currActivity = this.cordova.getActivity();
                this.currApp = this.cordova.getActivity().getApplication();
                this.cordova.getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        callbackContext.sendPluginResult(execOnThread(action, args));
                    }
                });
                return true;
            }
            //<#DEBUG_AREA>
            AttentionLogger( "BioCatch Plugin doesn't have method " + action + "()");
            //<#DEBUG_AREA>
        } catch (JSONException error) {
            error.printStackTrace();
            Log.e(TAG, "BioCatch Plugin error when parsing a parameter");
        } catch (Exception error) {
            error.printStackTrace();
            Log.e(TAG, "BioCatch Plugin error: " + error.getMessage());
        }
        return false;
    }

    private PluginResult execOnThread(String action, JSONArray args) {
        PluginResult.Status Status = PluginResult.Status.OK;
        String Message = null;
        String moreInfo = "";

        try {
            //<#DEBUG_AREA>
            AttentionLogger( "BioCatch API." + action + "()");
            //<#DEBUG_AREA>

            // Calling API.start()
            if (action.equals("start")) {
                String envArg = args.getString(0).toUpperCase();
                this.customerSessionId = args.getString(0);
                this.wuptUrl = args.getString(1);

                //<#DEBUG_AREA>
                SingleLogger( "Wups URL : " + this.wuptUrl);
                SingleLogger( "Customer Session ID : " + this.customerSessionId);
                SingleLogger( "BioCatch SDK Library version : " + API.getVersion());
                //</#DEBUG_AREA>

                API.start(this.currApp, this.currActivity, this.customerSessionId.toCharArray(), this.wuptUrl);
            }

            // Calling API.pause()
            if (action.equals("pause")) {
                API.pause();
            }

            // Calling API.resume()
            if (action.equals("resume")) {
                API.resume();
            }

            // Calling API.stop()
            if (action.equals("stop")) {
                API.stop();
            }

            // Calling FLUSH METHOD()
            if (action.equals("flush")) {
                //<#DEBUG_AREA>
                SingleLogger( "API.pause()" );
                //</#DEBUG_AREA>
                API.pause();
                //<#DEBUG_AREA>
                SingleLogger( "API.resume()" );
                //</#DEBUG_AREA>
                API.resume();
            }

            // Calling API.changeContext
            if (action.equals("changeContext")) {
                String contextName = args.getString(0);
                //<#DEBUG_AREA>
                SingleLogger( "Set Context to: " + contextName );
                //</#DEBUG_AREA>
                API.changeContext(contextName);
                moreInfo = "('" + contextName + "')";
            }

            // updateCustomerSessionID
            if (action.equals("updateCustomerSessionID")) {
                String newCustomerSessionId = args.getString(0);
                //<#DEBUG_AREA>
                SingleLogger( "Set NEW Customer Session ID to: " + newCustomerSessionId );
                //</#DEBUG_AREA>
                API.updateCustomerSessionID(newCustomerSessionId.toCharArray());
                this.customerSessionId = newCustomerSessionId;
                moreInfo = "('" + newCustomerSessionId + "')";
            }

            // resetSession
            if (action.equals("resetSession")) {
                API.resetSession();
            }

            Message = "Success: " + this.customerSessionId + " Action: " + action + moreInfo;

            //<#DEBUG_AREA>
            SingleLogger( Message );
            //</#DEBUG_AREA>
        } catch (JSONException error) {
            error.printStackTrace();
            Message = "Error Parsing: " + PluginResult.Status.JSON_EXCEPTION;
            Status = PluginResult.Status.JSON_EXCEPTION;
            Log.e(TAG, Message);
        } catch (Exception error) {
            error.printStackTrace();
            Message = "Error at : " + action + "() -> " + PluginResult.Status.ERROR + " Message: " + error.getMessage();
            Status = PluginResult.Status.ERROR;
            Log.e(TAG, Message);
        }
        return new PluginResult(Status, Message);
    }

    private String genCustomerSessionID() {
        String CSID = null;
        try {
            // Modify this part to generate a better CustomerSessionID string
            CSID = this.prefix + UUID.randomUUID().toString();
        } catch (Exception error) {
            //<#DEBUG_AREA>
            AttentionLogger("ERROR at resetSession() function" + error.getMessage());
            //</#DEBUG_AREA>
            error.printStackTrace();
        }
        return CSID;
    }
    
    //<#DEBUG_AREA>
    private void AttentionLogger(String msg) {
        Log.d(TAG, "===================================================");
        Log.d(TAG, msg);
        Log.d(TAG, "---------------------------------------------------");
    }

    private void SingleLogger(String msg) {
        Log.d(TAG, msg);
    }
    //</#DEBUG_AREA>

}