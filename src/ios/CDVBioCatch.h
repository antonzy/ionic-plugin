#import <Cordova/CDVPlugin.h>

@import BioCatchSDK;

@interface CDVBioCatch : CDVPlugin
- (void)start:(CDVInvokedUrlCommand*)command;
- (void)pause:(CDVInvokedUrlCommand*)command;
- (void)resume:(CDVInvokedUrlCommand*)command;
- (void)stop:(CDVInvokedUrlCommand*)command;
- (void)resetSession:(CDVInvokedUrlCommand*)command;
- (void)changeContext:(CDVInvokedUrlCommand*)command;
- (void)updateCustomerSessionID:(CDVInvokedUrlCommand*)command;
@end
