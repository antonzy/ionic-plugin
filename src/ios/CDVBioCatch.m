#import "CDVBioCatch.h"
#import <Cordova/CDV.h>

@interface CDVBioCatch () {
}
@end

@implementation CDVBioCatch

- (void)start:(CDVInvokedUrlCommand*)command
{
    [self logCommand:command];
    NSString* customerSessionID = [self argument:command atIndex:0];
    NSString* wupUrl = [self argument:command atIndex:1];
    NSString* publicKey = [self argument:command atIndex:2];

    if(!wupUrl){
        [self completeCommand:command withError:@"Missing wupUrl"];
        return;
    }

    [[BioCatch sharedInstance] startWithCustomerSessionID:customerSessionID bcServerUrl:[NSURL URLWithString:wupUrl] publicKey:publicKey];

    [self completeCommand:command];
}

- (void)pause:(CDVInvokedUrlCommand *)command
{
    [self logCommand:command];

    [[BioCatch sharedInstance] stop];

    [self completeCommand:command];
}

- (void)resume:(CDVInvokedUrlCommand *)command
{
    [self logCommand:command];

    [[BioCatch sharedInstance] resume];

    [self completeCommand:command];
}

- (void)stop:(CDVInvokedUrlCommand *)command
{
    [self logCommand:command];

    [[BioCatch sharedInstance] stop];

    [self completeCommand:command];
}

- (void)changeContext:(CDVInvokedUrlCommand *)command
{
    [self logCommand:command];

    NSString* contextName = [self argument:command atIndex:0];
    if (!contextName) {
        [self completeCommand:command withError:@"Missing contextName"];
        return;
    }

    [[BioCatch sharedInstance] changeContextWith:contextName];

    [self completeCommand:command];
}

- (void)updateCustomerSessionID:(CDVInvokedUrlCommand *)command
{
    [self logCommand:command];
    NSString* customerSessionID = [self argument:command atIndex:0];

    [[BioCatch sharedInstance] updateCustomerSessionID: customerSessionID];

    [self completeCommand:command];
}

- (void)resetSession:(CDVInvokedUrlCommand *)command
{
    [self logCommand:command];

    [[BioCatch sharedInstance] resetSession];

    [self completeCommand:command];
}

- (void)logCommand: (CDVInvokedUrlCommand*)command
{
    NSLog(@"[CDVBioCatch] %@ command (%@) recieved: %@", command.methodName, command.callbackId, command.arguments);
}

- (void)completeCommand: (CDVInvokedUrlCommand*)command
{
    NSLog(@"[CDVBioCatch] %@ command (%@) handled with success.", command.methodName, command.callbackId);
    [self.commandDelegate sendPluginResult: [CDVPluginResult resultWithStatus:CDVCommandStatus_OK] callbackId:command.callbackId];
}

- (void)completeCommand: (CDVInvokedUrlCommand*)command withError:(NSString*) error
{
    NSLog(@"[CDVBioCatch] %@ command (%@) handled with error. error: %@", command.methodName, command.callbackId, error);
    NSDictionary* errDict = @{@"message" : error};
    [self.commandDelegate sendPluginResult: [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary:errDict] callbackId:command.callbackId];
}

- (id)argument:(CDVInvokedUrlCommand*)command atIndex:(NSUInteger)index {
    id argument;

    @try {
        argument = [command.arguments objectAtIndex:index];
    }
    @catch (NSException *exception) {
        return nil;
    }

    return [[NSNull null] isEqual:argument] ? nil : argument;
}

@end
