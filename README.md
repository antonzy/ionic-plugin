# BioCatch Mobile SDK Cordova support
Plugin de BioCatch Mobile SDK para Ionic con soporte de plataformas iOS y Android / BioCatch Mobile SDK plugin for Ionic with support for iOS and Android platforms

**URL del repositorio / Repository URL** https://carlos_orellana@bitbucket.org/carlos_orellana/ionic-plugin.git

Translations
- [x] Español / Spanish
- [x] Ingles / English
- [ ] Portuguese
- [ ] Hebrew

## ¿Qué es un plugin de Cordova? / What is a Cordova plugin?
Cordova es un conjunto de herramientas que funcionan como puente para crear aplicaciones nativas e hibridas que se comunican a tráves de código Javascript. / Cordova is a set of tools that work as a bridge to create native and hybrid applications that communicate through Javascript code.

Ese puente nos permite hacer cosas sencillas o tan complejas nativas que no se incorporan a los estandares de la Web. / That bridge allows us to do simple or complex native things that are not incorporated into the Web standards.

> Construir plugins de Cordova significa qu estamos escribiendo algo de JavaScript para invocar a algún código nativo (Objetive-c, Java, entre otros) que tambien deberemos de escribir y devolver el resultado a nuestro JavaScript. / Building Cordova plugins means that we are writing some JavaScript to invoke some native code (Objective-c, Java, among others) that we must also write and return the result to our JavaScript.

En resumen, construimos un plugin de Cordova cuando queremos hacer algo nativo que aún no puede realizar el WebKit, como acceder a los datos de HealthKit, al scanner de huella, a la conexión bluetooth o a un SDK de terceros que permiten la conexión con dispositivos como impresoras y lectores. / In summary, we build a Cordova plugin when we want to do something native that the WebKit still cannot do, such as accessing HealthKit data, the fingerprint scanner, the bluetooth connection or a third-party SDK that allows connection with devices such as printers and readers.

En este caso particular nuestro plugin de BioCatch Mobile SDK nos permite acceder a las funcionalidades nativas de las librerias del SDK de BioCatch desde TypeScript. / In this particular case our BioCatch Mobile SDK plugin allows us to access the native functionalities of the BioCatch SDK libraries from TypeScrip

## Paso 1 / Step 1:
Posicionarse en el directorio de proyectos de Ionic en el cual va a incluir la integración con BioCatch / Go to Ionic project directory in which you will include integration with BioCatch`

Si no tiene un proyecto puede crear uno con el siguiente comando / If you don't have a project you can create one with the following command: `ionic start ProyectoPrueba blank`

Seleccionar proyecto / Select project: `cd ProyectoPrueba/`

## Paso 2 / Step 2:
Agregar el plugin a cordova / Add the plugin to cordova: `ionic cordova plugin add cordova-plugin-biocatch`

## Paso 3 / Step 3:
Agregar el plugin nativo de ionic / Add the native ionic plugin: `npm install @ionic-native/biocatch`

## Paso 4 / Step 4:
Incluir la referencia al plugin de BioCath en su app.modules.ts / Include the reference to the BioCath plugin in your app.modules.ts:
```
import { BioCatch } from '@ionic-native/biocatch/ngx';
...
@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    BioCatch
  ],
  bootstrap: [AppComponent]
})
```
## Paso 5 / Step 5:
Incluir luego en los modulos de su aplicacion las llamadas a nuestras rutinas de la siguiente manera / Then include the calls to our routines in the modules of your application as follows::

```
....
import { BioCatch } from '@ionic-native/biocatch/ngx';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  public customerSessionID = 'VALUE_FOR_CUSTOMER_SESSION_ID';

  constructor(private bioCatch: BioCatch, private toastController: ToastController) {
  }

  public async start() {
    const wupUrl = 'https://YOUR_SERVER_URL/client/v3/web/wup?cid=YOUR_CUSTOMER_ID';

    await this.bioCatch.start(this.customerSessionID, wupUrl, null);

    this.presentToast('Data collection started!');
  }

  presentToast(message: string) {
    this.toastController.create({message, duration: 1500, color: 'medium'}).then(toast => toast.present());
  }
}
```

## Paso 6 / 6:
Ejecutar en iOS / To execute on iOS: `ionic cordova run ios --prod` o para Android / or Android: `ionic cordova run android --prod`.

Posteriormente puedes ir al directorio **platforms** donde encontrarás la carpeta de cada sistema operativo y podrás abrirlo en Xcode o Android Studio compilar y correr la aplicación. / Later you can go to the directory **platforms** where you will find the folder of each operating system and you can open it in Xcode or Android Studio to compile and run the application.

**NOTA / NOTE:** En tu proyecto podrás observar que dentro de la carpeta de **plugins** encontrarás la carpeta **biocatch-plugin** que a su vez contiene todos los elementos para ambas plataformas iOS y Android. / In your project you can see that within the **plugins** folder you will find the  **biocatch-plugin** folder which contains all the elements for both iOS and Android platforms.




